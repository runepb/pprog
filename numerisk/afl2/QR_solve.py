import numpy as np


def QR_solve(Q, R, b):
    c = np.dot(np.transpose(Q), b)
    n = len(c)
    x = np.zeros(n)

    for i in reversed(range(n)):
        ci = c[i]
        for j in range(i+1, n):
            ci -= R[i,j] * x[j]

        x[i] = ci / R[i,i]
    return x