import numpy as np
from math import *

class GS_QR:
	def calc(self,a):
		n,m=np.shape(a)
		q=np.copy(a)
		r=np.zeros((m,m))
		for i in range(m):
			r[i,i]=np.linalg.norm(q[ : ,i])
			q[ : ,i]=q[ : ,i]/r[i,i]
			for j in range(i+1,m):
				innerProduct = np.dot(q[:,i],q[:,j])
				q[:,j]=q[:,j] - innerProduct * q[:,i]
				r[i,j] = innerProduct
				
		return q,r
						
	def innerProduct(self,a,b):
		n=np.shape(a)[0]
		innerProduct = 0
		for k in range(n):
			innerProduct += a[k]*b[k]
		return innerProduct
