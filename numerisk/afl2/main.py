import sys
import numpy as np
import random
from GS_QR import GS_QR
import QR_solve

n=5
m=3
A=np.zeros((n,m))
for i in range(n):
	for j in range(m) :
		A[i,j]=random.random();
print("matrix A=\n",A)
tes=GS_QR()
(Q,R)=tes.calc(A)
print("R=\n",R)
print("Q=\n",Q)

Z=np.dot(Q.transpose(),Q)
print("Q.t*Q=",Z)
I=np.identity(np.shape(Z)[1])
if(  Z-I < 1e-12).all() : print("Q.t*Q is equal to I")
else       : print("Q.t*Q is not equal to I")

Z=np.dot(Q,R)
print("QR=\n",Z)
if(  Z-A< 1e-12).all() : print("QR is equal to A")
else       : print("QR is not equal to A")

n = 5
A = np.random.rand(n, n) * 10
b = np.random.rand(n) * 5

print("\n Matrix A=\n",A)
print("\n Vector b=\n",b)

QR=GS_QR()
Q, R = QR.calc(A)
x = QR_solve.QR_solve(Q, R, b)
print("x = \n",x)

if (np.dot(A, x) - b < 1e-12).all():
	print('AX=b is success!')
else:
	print('AX=B is false ')
print("\nAx = ")
print(np.dot(A, x))






# b = np.random.rand(n) * 5
# print("\nSolving Ax = b for x...")
#
# x = QR_solve.QR_solve(Q, R, b)
# print("Done! x = ")
# print(x)
#
# # assert (np.dot(A, x) - b < 1e-12).all()
# print("\nAx = ")
# print(np.matmul(A, x))
# print("\nb = ",b)
