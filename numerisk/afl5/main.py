import Roots
import numpy as np
import scipy.optimize
def rosenbrock(z):
    global ncalls
    ncalls+=1
    f = np.zeros(2)
    x = z[0]
    y = z[1]
    f[0] = -2 * (1 - x) - 400* (y - x ** 2) * x
    f[1] = 200 * (y - x ** 2)
    return f


def dRosenbrock(z):
    global ncalls
    ncalls+=1
    J = np.zeros([2, 2])
    x = z[0]
    y = z[1]
    J[0, 0] = 2 + 800 * x ** 2 - 400 * (y - x ** 2)
    J[1, 0] = -400 * x
    J[0, 1] = -400 * x
    J[1, 1] = 200
    return J;


def himmelblau(x):
    f = np.empty(2)
    f[0] = 2 * (2 * x[0] * (x[0] ** 2 + x[1] - 11) + x[0] + x[1] ** 2 - 7)
    f[1] = 2 * (x[0] ** 2 + 2 * x[1] * (x[0] + x[1] ** 2 - 7) + x[1] - 11)
    return f


def fA(z):
    global ncalls
    ncalls+=1
    f = np.zeros(2)
    x = z[0]
    y = z[1]
    A = 10000
    f[0] = A * x * y - 1
    f[1] = np.exp(-x) + np.exp(-y) - 1 - 1 / A
    return f


def dfA(z):
    global ncalls
    ncalls+=1
    J = np.zeros([2, 2])
    x = z[0]
    y = z[1]
    A = 10000
    J[0, 0] = A * y
    J[0, 1] = A * x
    J[1, 0] = np.exp(-x) * (-1)
    J[1, 1] = np.exp(-y) * (-1)
    return J


if __name__ == '__main__':

    print("Root finding")
    xStart = np.array([2, 1])
    print("Starting points = ", xStart)

    print("\n Functions of A")
    print("By numerical jacobian")
    ncalls=0
    solution = Roots.newton(fA, xStart, eps=1e-4,dx=1e-3)
    print("Found solution = ", solution)
    print('number of calls',ncalls)

    print("By analytical jacobian")
    ncalls=0
    solution = Roots.newtonJacobian(fA, dfA, xStart)
    print('Solution found', solution)
    print('number of calls',ncalls)

    print("By scipy.optimize.root")
    ncalls=0
    solution = scipy.optimize.root(fA, xStart, jac=dfA)
    print("Found solution = ", solution.x)
    print('number of calls',ncalls)


    print("\nRosenbrock function:")

    print("By numerical jacobian")
    ncalls=0
    solution = Roots.newton(rosenbrock, xStart)
    print("Solution found = ", solution)
    print('number of calls',ncalls)

    print("By analytical jacobian")
    ncalls=0
    solution = Roots.newtonJacobian(rosenbrock, dRosenbrock, xStart)
    print('Solution found',solution)
    print('number of calls',ncalls)

    print("By scipy.optimize.root")
    ncalls=0
    solution = scipy.optimize.root(rosenbrock, xStart, jac=dRosenbrock)
    print("Found solution = ", solution.x)
    print('number of calls',ncalls)

    print("\nHimmelblau function:")
    ncalls=0
    solution = Roots.newton(himmelblau, xStart, 1e-6)
    print("Solution found = \n", solution)
