import sys

sys.path.append('../afl2')
import GS_QR
import QR_solve
import numpy as np


def newton(f, xGuess, eps=1e-4, dx=1e-6):
    x = np.copy(xGuess).astype(float)
    k=0
    n = np.shape(x)[0]
    J = np.zeros([n, n])
    shouldStop=False
    while not shouldStop:
        k+=1
        fx = f(x)
        for j in range(n):
            x[j] += dx
            df = f(x) - fx
            for i in range(n):
                J[i, j] = df[i] / dx
            x[j] -= dx
        QR=GS_QR.GS_QR()
        (Q,R)=QR.calc(J)
        Dx = QR_solve.QR_solve(Q,R, -fx)
        s = 2
        lambdaFound=False
        while not lambdaFound:
            s /= 2
            y = x + Dx * s
            fy = f(y)
            lambdaTooSmall = s < 0.02
            lambdaReachedCorrectSize =np.linalg.norm(fy) < (1 - s / 2) * np.linalg.norm(fx)
            lambdaFound = lambdaReachedCorrectSize or lambdaTooSmall

        x = y;
        fx = fy
        tooSmallDx = np.linalg.norm(Dx) < dx
        underTolerance=np.linalg.norm(fx) < eps
        shouldStop = tooSmallDx or underTolerance
    return x;


def newtonJacobian(f, jacobian, xGuess, eps= 1e-4):
    x = np.copy(xGuess).astype(float)
    while True:
        fx = f(x)
        J = jacobian(x)
        QR=GS_QR.GS_QR()
        (Q,R)=QR.calc(J)
        Dx = QR_solve.QR_solve(Q,R, -fx)
        s = 2
        while True:
            s /= 2
            y = x + Dx * s
            fy = f(y)
            if np.linalg.norm(fy) < (1 - s / 2) * np.linalg.norm(fx) or s < 0.02: break
        x = y;
        fx = fy;
        if np.linalg.norm(fx) < eps: break
    return x;
