import sys
sys.path.append('../afl2')
import GS_QR
import QR_solve
import numpy as np


def newton(func,dfunc,Hfunc, point, eps=1e-7,):
    x = np.copy(point).astype(float)
    alpha = 1e-2
    steps = 0
    underTolerance = False
    while not underTolerance:
        steps += 1
        fx=func(x)
        dfdx = dfunc(x)
        H = Hfunc(x)
        QR=GS_QR.GS_QR()
        (Q,R)=QR.calc(H)
        Dx = QR_solve.QR_solve(Q,R, -dfdx)
        l = 1
        lambdaFound = False
        while not lambdaFound:
            s = l * Dx
            x2 = x + s
            fx2 = func(x2)
            dfdx2 = dfunc(x2)
            H = Hfunc(x2)
            conditionSatisfied=fx2 < fx + alpha * np.dot(np.transpose(s), dfdx)
            lambdaTooSmall =l < 1 / 64
            lambdaFound=conditionSatisfied or lambdaTooSmall
            l = l / 2
        x = x2
        dfdx = dfdx2
        underTolerance = np.linalg.norm(dfdx) < eps
    return x, steps