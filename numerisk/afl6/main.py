import numpy as np
import minimization as min
from matplotlib import pyplot as plt


def rosenbrock(xStart):
    a = 1
    b = 100
    x = xStart[0]
    y = xStart[1]
    z = (a-x)**2 + b*(y-x**2)**2
    return z

def dRosenbrock(xStart):
    x = xStart[0]
    y = xStart[1]
    dfdx = np.zeros(2)
    dfdx[0] = 2*(200*x**3-200*x*y+x-1)
    dfdx[1] = 100 * 2 * (y - x ** 2)
    return dfdx

def HRosenbrock(xStart):
    n = xStart.size
    x = xStart[0]
    y = xStart[1]
    H = np.zeros([n, n])
    H[0, 0] = 400*(3*x**2-y)+2
    H[0, 1] = -400*x
    H[1, 0] = H[0, 1]
    H[1, 1] = 200
    return  H


def himmelblau(xStart):
    a = 7; b = 11
    x = xStart[0]
    y = xStart[1]
    z = (x**2+y-b)**2 + (x+y**2-a)**2
    return z

def HHimmelblau(xStart):
    x = xStart[0]
    y = xStart[1]
    n = np.shape(xStart)[0]
    H = np.zeros([n, n])
    H[0, 0] = 12*x**2 + 4*y - 42
    H[0, 1] = 4*(x+y)
    H[1, 0] = H[0, 1]
    H[1, 1] = 4*x + 12*y**2 - 26
    return H

def dHimmelblau(xStart):
    x = xStart[0]
    y = xStart[1]
    dfdx = np.zeros(2)
    dfdx[0] = 2 * (2 * x * (x ** 2 + y - 11) + x + y ** 2 - 7)
    dfdx[1] = 2 * (x ** 2 + 2 * y * (x + y ** 2 - 7) + y - 11)
    return dfdx

if __name__ == '__main__':
    print("newton minimization of Rosenbrock and Himmelblau.")
    xStart = np.array([18.0, 15.0])
    q, steps = min.newton(rosenbrock, dRosenbrock,HRosenbrock,xStart,eps=1e-4)
    print("Rosenbrock minimum =", q)
    print("Number of steps =", steps)

    q, steps = min.newton(himmelblau, dHimmelblau, HHimmelblau, xStart,eps=1e-4)
    print("\nHimmelblau minimum =", q)
    print("Number of steps =", steps)