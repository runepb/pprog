import numpy as np
import scipy.optimize as optim


class ANN1D:
    def __init__(self, num_neurons, function):
        self.n = num_neurons
        self.f = function
        self.params = np.reshape(np.random.rand(3 * self.n),(self.n,3))
    def feed_forward(self, input):
        x = input
        output = 0
        for i in range(self.n):
            a = self.params[i][0]
            b = self.params[i][1]
            w = self.params[i][2]
            output += self.f((x + a) / b) * w
        return output

    def train(self, input_values, exact_values):

        def delta(p):
            self.params = np.reshape(p, (self.n, 3))
            f = exact_values
            y = self.feed_forward(input_values)
            s = np.sum(np.abs(y - f) ** 2)
            return s

        optim.minimize(delta, self.params, method='BFGS', tol=1e-3)


class ANN2D:
    def __init__(self, num_neurons, function):

        self.n = num_neurons
        self.f = function
        self.params = np.reshape(np.random.rand(5 * self.n),(self.n, 5))

    def feedForward(self, input):
        x = input[0]
        y = input[1]
        output = 0
        for i in range(self.n):
            a = self.params[i][0]
            b = self.params[i][1]
            c = self.params[i][2]
            d = self.params[i][3]
            w = self.params[i][4]
            output += self.f((x + a) / b, (y - c) / d) * w
        return output

    def train(self, inputValues, exactValues):
        def delta(p):
            self.params = np.reshape(p, (self.n, 5))
            f = exactValues
            z = self.feedForward(inputValues)
            s = np.sum(np.abs(z - f) ** 2)
            return s
        optim.minimize(delta, self.params, method='BFGS', tol=1e-3)


