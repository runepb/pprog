import numpy as np
import ODE
from matplotlib import pyplot as plt


def F(t, y):
    return np.array([y[1], -y[0]])


if __name__ == '__main__':
    a = 0
    b = 4 * np.pi
    eps = 0.01
    acc = 0.01
    hstart = 0.1
    t = np.array([0])
    y=[np.array([0, 1])]
    tlist, ylist = ODE.rkdriver(F, t, y, b, hstart, eps, acc)

    print('time, y[0], y[1]')
    for i in range(len(tlist)):
        print(tlist[i], ylist[i][0], ylist[i][1])
    plt.plot(tlist, ylist)
    plt.xlabel("x")
    plt.ylabel("y")
    plt.title("Sinus and Cosinus by ODE")
    plt.savefig("plot.svg")
