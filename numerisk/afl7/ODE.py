import numpy as np


def rkstep12(F, t, y, h):
    k0 = F(t, y)
    k1 = F(t + np.ones(t.size)*h/2, y + k0 * (h/2))
    yh = y + k1 * h #y after step
    ys = y + k0 * h #y* to calc err
    err = np.linalg.norm((yh-ys))
    return yh, err


def rkdriver(F, tlist, ylist, b, step=1e-2, eps=1e-6, acc=1e-6):
    a = tlist[-1]
    nsteps = 0
    finished = False
    while not finished :
        t0 = tlist[-1]
        y0 = ylist[-1]
        if t0 + step > b:
            step = b - t0
        (y, err) = rkstep12(F, t0, y0, step)
        tol = (acc + np.linalg.norm(y)*eps) * np.sqrt(step/(b-a))
        if err < tol:
            nsteps += 1
            tlist = np.append(tlist, (t0+step))
            ylist.append(y)
        if err == 0:
            step *= 2
        else:
            step *= ((tol/err)**0.25) * 0.95
        nsteps < 999
        timeIsUp = tlist[-1] >= b
        stepIsDone = nsteps>999
        finished = timeIsUp or stepIsDone

    return tlist, ylist
