import numpy as np
from matplotlib import pyplot as plt
import lsf
from math import *

def p0(z): return np.log(z)
def p1(z): return 1.0
def p2(z): return z


if __name__ == '__main__':
    functions = [p0, p1, p2]
    x = [0.1, 1.33, 2.55, 3.78, 5, 6.22, 7.45, 8.68, 9.9]
    y = [-15.3, 0.32, 2.45, 2.75, 2.27, 1.35, 0.157, -1.23, -2.75]
    dy = [1.04, 0.594, 0.983, 0.998, 1.11, 0.398, 0.535, 0.968, 0.478]

    print("Data to be fitted")
    print("x = \n", x)
    print("y = \n", y)
    print("error_y =\n ", dy)
    c, S = lsf.calc(functions, x, y, dy)
    print("Coeffictions from least square =\n",c)

    print("\nCovariance matrix = \n",S)
    print("\nResulting in these coeffitients")
    for i in range(3):
        print("c",i," = " + str(c[i]) + " ± " + str(sqrt(S[i, i])))

    xvalues = np.linspace(0, 10, 100)
    F = np.empty(len(xvalues))
    for i in range(len(xvalues)):
        F[i] = c[0]*functions[0](xvalues[i]) + c[1]*functions[1](xvalues[i]) + c[2]*functions[2](xvalues[i])

    plt.errorbar(x, y, dy, label="Data", fmt='.')
    plt.plot(xvalues, F, label="least square fit")
    plt.xlabel("x")
    plt.ylabel("y")
    plt.legend()
    plt.savefig("plot.svg")
