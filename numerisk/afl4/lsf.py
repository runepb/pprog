import sys
sys.path.append('../afl2')
import GS_QR
import QR_solve
import numpy as np


def calc(functions, x, y, dy):
    n = len(x)
    m = len(functions)
    A = np.zeros([n, m])
    b = np.zeros(n)
    for i in range(n):
        b[i] = y[i] / dy[i]
        for k in range(m):
            A[i, k] = functions[k](x[i]) / dy[i]
    QR1=GS_QR.GS_QR()
    (Q, R) = QR1.calc(A)
    c = QR_solve.QR_solve(Q, R, b)
    R_I= np.linalg.inv(R)
    S = np.dot(R_I, R_I.transpose())
    return (c, S)