import numpy as np
from BinSearch import BinSearch
from math import *
class Quad:
	def __init__(self,x,y):
		self.x=x
		self.y=y

		
	def spline(self,z):
		n=len(self.x)
		p=np.zeros(n-1)
		b=np.zeros(n-1)
		c=np.zeros(n-1)
		Dy=np.zeros(n-1)
		Dx=np.zeros(n-1)
		
		for i in range(n-1) :
			Dy[i]=(self.y[i+1]-self.y[i])
			Dx[i]=(self.x[i+1]-self.x[i])
			p[i]=Dy[i]/Dx[i]
			
		for i in range(n-2) :
			c[i+1]=(p[i+1]-p[i]-c[i]*Dx[i])/Dx[i+1]
		c[-1]/=2; #recursion down
		for i in reversed(range(n-2)) :
			c[i]=(p[i+1]-p[i]-c[i+1]*Dx[i+1])/Dx[i] # Gennemsnit, er ikke sikker.
		for i in range(n-1) :
			b[i]=p[i] - c[i] * Dx[i]
		self.b=b
		self.c=c
		i = 0
		j = len(self.x)-1
		while j-i > 1:
			m = floor((i+j)/2)
			if z > self.x[m]:
				i = m
			else:
				j = m
		result=self.y[i]+b[i]*(z-self.x[i])+c[i]*(z-self.x[i])**2
		return result
		
	def intergrate(self,z) :
		result = 0
		i = BinSearch(self.x,self.y,z)
		qSpline=self.spline(z)
		b = self.b
		c = self.c
		for j in range(i):
			Dx = self.x[j+1] - self.x[j]
			result += (self.y[j]*Dx + (1/2)*b[j]*Dx**2 + (1/3)*c[j]*Dx**3 )
		i = 0
		j = len(self.x)-1
		while j-i > 1:
			m = floor((i+j)/2)
			if z > self.x[m]:
				i = m
			else:
				j = m

		Dx=z - self.x[i]
		result += (self.y[i]*Dx + (1/2)*b[i]*Dx**2 + (1/3)*c[i]*Dx**3)
		return result

	
	def differentiate(self,z):
		qSpline=self.spline(z)
		b = self.b
		c = self.c
		i = 0
		j = len(self.x)-1
		while j-i > 1:
			m = floor((i+j)/2)
			if z > self.x[m]:
				i = m
			else:
				j = m

		Dx = z-self.x[i]
		result=b[i] + 2 * c[i] * Dx
		return result 
