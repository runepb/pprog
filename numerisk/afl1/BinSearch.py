import math
def BinSearch(x, y, z):
	assert len(x)==len(y)
	i=0
	j=len(x)-1
	while j-i>1 :
		m=math.floor((i+j)/2)
		if z>=x[m] : i=m
		else	: j=m
	return i
