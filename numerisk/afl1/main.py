import random,math
from Quad import Quad
from Linear import Linear
def main():
	random.seed(42)
	N=10
	x=[ i for i in range(N)]
	y=[ math.cos(i)   for i in range(N)]

	for i in range(len(x)) : print(x[i]," ",y[i])

	print("\n\n")
	ls=Linear(x,y)
	qs=Quad(x,y)
	z=x[0]
	while z<=x[-1] :
		print(z," ",ls.spline(z)," ",qs.spline(z)," ",ls.intergrate(z)," ",qs.intergrate(z)," ",qs.differentiate(z))
		z=z+0.1
main()
