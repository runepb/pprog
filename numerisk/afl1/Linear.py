import math
from BinSearch import BinSearch
from math import *
class Linear :
	def __init__(self,x,y):
		self.x=x
		self.y=y
	
	def spline(self,z):
		i = 0
		j = len(self.x)-1
		while j-i > 1:
			m = floor((i+j)/2)
			if z > self.x[m]:
				i = m
			else:
				j = m
		slope =(self.y[i+1]-self.y[i])/(self.x[i+1]-self.x[i])
		result = self.y[i]+(z-self.x[i])*slope
		return result

	def intergrate(self,z):
		result=0
		i=BinSearch(self.x,self.y,z)
		i = 0
		j = len(self.x)-1
		while j-i > 1:
			m = floor((i+j)/2)
			if z > self.x[m]:
				i = m
			else:
				j = m
		for j in range(i): #calculates area under each interval j to j+1 as a box and a triangle
			dx = self.x[j+1]-self.x[j]
			dy = self.y[j+1]-self.y[j]
			box=dx*self.y[j]
			triangle=1/2 * dx * dy
			result+=box+triangle
		yEnd=self.spline(z)
		dx=z-self.x[i]
		dy=yEnd-self.y[i]
		box=dx*self.y[i]
		triangle=1/2 * dx * dy
		result+=box+triangle
		return result
