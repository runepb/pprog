import numpy as np

def calc(f, a, b, N):
    volume = 1
    sum1 = 0
    sum2 = 0
    x = np.zeros(a.shape, dtype='float64')
    for i in range(len(a)):
        volume *= b[i] - a[i]

    for i in range(N):
        x = np.random.uniform(a, b, len(a))
        fx = f(x)
        sum1 += fx
        sum2 += fx **2
    mean = sum1 / N
    sigma = np.sqrt(sum2 / N - mean * mean)

    error = volume * sigma / np.sqrt(N)
    integ = mean * volume

    return integ, error

