import math
import numpy as np

def eval(A, e, V):
    n = np.shape(A)[0]
    for i in range(n):
        e[i] = A[i, i]
    sweeps = 0
    changed = True
    V[:,:] = np.identity(n)
    while changed:
        changed = False
        sweeps += 1
        for q in reversed(range(n)):
            for p in range(q):
                App = e[p]
                Aqq = e[q]
                Apq = A[p, q]
                Aqp = A[p, q]
                phi = 0.5*math.atan2(2*Apq,Aqq-App)
                c = math.cos(phi)
                s = math.sin(phi)

                App1 = c ** 2 * App + s ** 2 * Aqq - 2 * c * s * Apq
                Aqq1 = s ** 2 * App + c ** 2 * Aqq + 2 * c * s * Apq
                if (App1 != App or Aqq1 != Aqq):
                    changed = True
                    e[p]=App1
                    e[q]=Aqq1
                    A[p,q]= 0
                    for i in range(p):
                        aip = A[i, p]
                        aiq = A[i, q]
                        A[i, p] = c * aip - s * aiq
                        A[i, q] = c * aiq + s * aip
                    for i in range(p + 1, q):
                        api = A[p, i]
                        aiq = A[i, q]
                        A[p, i] = c * api - s * aiq
                        A[i, q] = c * aiq + s * api
                    for i in range(q + 1, n):
                        api = A[p, i]
                        aqi = A[q, i]
                        A[p, i] = c * api - s * aqi
                        A[q, i] = c * aqi + s * api
                    for i in range(n):
                        vip = V[i, p]
                        viq = V[i, q]
                        V[i, p] = c * vip - s * viq
                        V[i, q] = c * viq + s * vip
    for i in range(n):
        for k in range(n):
            if A[i,k]<0.0001:
                A[i,k]=0
    return sweeps;
