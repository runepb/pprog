from __future__ import print_function
import sys
import string
import numpy as np
import random
import eigen
import math

if len(sys.argv) > 1:
    n = int(sys.argv[1])
else:
    n = 5;
max_print = 6;

random.seed(1)

A = np.zeros([n, n]);
V = np.identity(n);
e = np.zeros(n);
for i in range(n):
    A[i, i] = random.random();
    for j in range(i + 1, n):
        A[i, j] = random.random()
        A[j, i] = A[i, j]
if n > max_print:
    sweeps = eigen.eval(A, e, V);
    print("e0=", e[0], " sweeps=", sweeps, file=sys.stderr)
else:
    print("Jacobi diagonalization with cyclic sweeps");
    print("----------------------");
    print("n={}".format(n));

    print("A random symmetric matrix A:",A);
    AA = np.copy(A);
    sweeps = eigen.eval(A, e, V);
    print("sweeps: {}".format(sweeps));
    print("V",V);
    print("matrix A after diagonalization (upper triangle should be zeroed):\n",A);
    diag = (np.dot(np.dot(V.transpose(), AA), V))
    for i in range(n):
        for k in range(n):
            if abs(diag[i,k])<0.0001:
                diag[i,k]=0
    print("V^T*A*V (should be diagonal):\n",diag);
    print("eigenvalues (should be eqal the diagonal elements of V^T*A*V):\n",e,"\n");
