import numpy as np
x=1
def open4(f, a, b, acc=1e-3, eps=1e-3):
    F = f
    if a is np.NINF and b is not np.PINF:
        f = lambda t: (F(b - (1 - t)/t))/t**2
        a = 0
        b = 1
    elif a is not np.NINF and b is np.PINF:
        f = lambda t: (F(a + (1 - t)/t))/t**2
        a = 0
        b = 1
    elif a is np.NINF and b is np.PINF:
        f = lambda t: (F((1 - t)/t) + F(- (1 - t)/t))/t**2
        a = 0
        b = 1
    f2 = f(a + 1. / 3 * (b - a))
    f3 = f(a + 2. / 3 * (b - a))
    return open4_reuse(f, a, b, acc, eps, f2, f3)


def open4_reuse(f, a, b, acc, eps, f2, f3):
    f1 = f(a + 1. / 6 * (b - a))
    f4 = f(a + 5. / 6 * (b - a))
    w = [2 / 6, 1 / 6, 1 / 6, 2 / 6]
    v = [1 / 4, 1 / 4, 1 / 4, 1 / 4]
    Q = (w[0]*f1 + w[1]*f2 + w[2]*f3 + w[3] * f4) * (b - a)
    q = (v[0]*f1 + v[1]*f2 + v[2]*f3 + v[3] * f4) * (b - a)
    tol = acc + abs(Q) * eps
    err = abs(Q - q)
    if err < tol:
        return Q
    else:
        Q1 = open4_reuse(f, a, (a + b) / 2., acc / np.sqrt(2), eps, f1, f2)
        Q2 = open4_reuse(f, (a + b) / 2., b, acc / np.sqrt(2), eps, f3, f4)
        return Q1 + Q2


def clenshaw_curtis(f: 'integrand', a: float, b: float, acc: float = 1e-3, eps: float = 1e-3):
    middleOfInterval=(a + b) / 2
    lengthOfInterval=(b-a)
    #these are used for making the original limits into -1 and 1, where cos and sin can then be used for the Clenshaw.
    g = lambda t: f(middleOfInterval+lengthOfInterval / 2 * np.cos(t)) * np.sin(t) * lengthOfInterval / 2
    #Lambda is a smart way of making a short lived anonymous function. I did not know this method before reading Demitry's code, but i thourght it smart.
    return open4(g, 0, np.pi, acc, eps)
