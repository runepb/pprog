import numpy as np
import Integrate
import sys
from matplotlib import pyplot as plt

ncalls = 0


def f(x):
    global ncalls
    ncalls += 1
    return np.sqrt(x)

def printer(title,function,a,b,acc,eps,Q,exact):
    fmt = "{:g}"
    print(title)
    print("integrating ",function," from ", a, " to ", b, " :")
    print("acc             = ", acc)
    print("eps             = ", eps)
    print("ncalls          = ", ncalls)
    print("integral        = ", Q)
    print("exact           = ", exact)
    print("estimated error : ", fmt.format(acc + abs(Q) * eps))
    print("actual error    : ", fmt.format(abs(Q - exact)), "\n")


sys.setrecursionlimit(15000)

a = 0;
b = 1;
acc = 0.0001;
eps = 0.0001;

ncalls = 0;
Q = Integrate.open4(f, a, b, acc, eps);
exact = 2 / 3;
title = ("Adaptive open4 integration:")
function = ("sqrt(x)")
printer(title,function,a,b,acc,eps,Q,exact)


def fp(x):
    global ncalls;
    ncalls += 1
    return 4 * np.sqrt(1 - (1 - x) * (1 - x))


acc = 1e-6;
eps = 1e-6;
ncalls = 0;
Q = Integrate.open4(fp, a, b, acc, eps);
exact = np.pi;
title = ("Adaptive open4 integration:")
function = ("4*sqrt(1-(1-x)*(1-x))")
printer(title,function,a,b,acc,eps,Q,exact)

acc = 1e-6;
eps = 1e-6;
ncalls = 0;
Q = Integrate.clenshaw_curtis(fp, a, b, acc, eps);
exact = np.pi;
title = ("Clenshaw-Curtis integration:")
function = ("4*sqrt(1-(1-x)*(1-x))")
printer(title,function,a,b,acc,eps,Q,exact)

def f2(x):
    global ncalls;
    ncalls += 1
    return np.log(x) / np.sqrt(x)


acc = 1e-3;
eps = 1e-3;
ncalls = 0;
Q = Integrate.open4(f2, a, b, acc, eps);
exact = -4;
title = ("Adaptive open4 integration:")
function = "log(x)/sqrt(x)"
printer(title,function,a,b,acc,eps,Q,exact)
acc = 1e-3;
eps = 1e-3;
ncalls = 0;
Q = Integrate.clenshaw_curtis(f2, a, b, acc, eps);
exact = -4;
title = "Clenshaw_Curtis integration:"
function = "log(x)/sqrt(x)"
printer(title,function,a,b,acc,eps,Q,exact)


def f3(x):
    global ncalls;
    ncalls += 1
    return 1 / np.sqrt(x)


acc = 1e-3;
eps = 1e-3;
ncalls = 0;
Q = Integrate.open4(f3, a, b, acc, eps);
exact = 2;
title = "Adaptive open4 integration:"
function = "1/sqrt(x)"
printer(title,function,a,b,acc,eps,Q,exact)

acc = 1e-3;
eps = 1e-3;
ncalls = 0;
exact = 2;
Q = Integrate.clenshaw_curtis(f3, a, b, acc, eps);
title="Clenshaw-Curtis integration:"
function="1/sqrt(x)"
printer(title,function,a,b,acc,eps,Q,exact)


def fe(x):
    global ncalls;
    ncalls += 1
    return np.exp(x)


a = np.NINF
b = 1;
acc = 1e-4;
eps = 0;
exact = np.exp(1);
Q = Integrate.open4(fe, a,b, acc, eps)
title="Adaptive open4 integration:"
function="exp(x)"
printer(title,function,a,b,acc,eps,Q,exact)


def fe(x):
    global ncalls;
    ncalls += 1
    return np.exp(-x)


a = 0
b = np.PINF;
acc = 1e-4;
eps = 0;
exact = 1;
Q = Integrate.open4(fe, a,b, acc, eps)
title="Adaptive open4 integration:"
function="exp(x)"
printer(title,function,a,b,acc,eps,Q,exact)

def fe(x):
    global ncalls;
    ncalls += 1
    return (np.exp(-x**2))


a = np.NINF
b = np.PINF;
acc = 1e-4;
eps = 0;
exact = np.sqrt(np.pi);
Q = Integrate.open4(fe, a,b, acc, eps)
title="Adaptive open4 integration:"
function="exp(-x²)"
printer(title,function,a,b,acc,eps,Q,exact)