import numpy as np
from math import *
import MonteCarlo
import sys
from inspect import getsource
from matplotlib import pyplot as plt
from scipy.optimize import curve_fit
import time


def f1(x):
    return np.sqrt(x)

def f2(x):
    return np.sin(x[0]) * x[1]

def f3(x):
    return 1/(pi**3*(1-cos(x[0])*sin(x[1])*cos(x[2])))


def fit_func(x, a):
    return a * 1/np.sqrt(x)

def printer(function,a,b,N,Q,exact,err):
    fmt = "{0:.5f}"
    # print("integrating ",getsource(function)," from ", a, " to ", b, " :")
    print("integral        = ", fmt.format(Q))
    print("exact           = ", fmt.format(exact))
    print("estimated error = ", fmt.format(err))
    print("Real error      = ", fmt.format(abs(Q-exact)))
def main_A():
    f = ([f1, f2, f3])
    a = np.array([np.array([1]), np.array([0, 0]), np.array([0, 0, 0])])
    b = np.array([[16],[np.pi/2.,np.pi], [np.pi, np.pi, np.pi]])
    N = ([10000, 10000, 10000])
    exact = ([42, (np.pi*np.pi)/2, 1.3932039296856768591842462603255])
    # Integrate (part a)
    for i in range(len(f)):
        Q,err= MonteCarlo.calc(f[i], a[i], b[i], N[i])
        print(Q)
        print(err)
        printer(getsource(f[i]),a,b,N,np.asscalar(Q),exact[i],np.asscalar(err))

def main_B():
    fMid = ([f1, f2])
    aMid = np.array([np.array([1]), np.array([0, 0])])
    bMid = np.array([[16],[np.pi/2.,np.pi]])
    NMid = ([10000, 10000])
    for j in range(len(NMid)):
        N = np.arange(10, NMid[j], 60)
        error = np.zeros(N.size)
        a = aMid[j]
        b = bMid[j]
        f = fMid[j]
        for i in range(N.size):
            Q, err = MonteCarlo.calc(f, a, b, N[i])
            error[i] = err
        fit_params, pcov = curve_fit(fit_func, N, error)
        plt.plot(N, error,linestyle='none', marker='.', label="Simulation data")
        fit_string = "fit = %.3f *1/sqrt(x)" % fit_params[0]
        plt.plot(N, fit_func(N, fit_params[0]), label=fit_string)
        plt.xlabel("N")
        plt.ylabel("Error")
        plt.legend()
    plt.savefig("plot.svg")


if __name__ == '__main__':
    main_A()
    main_B()
