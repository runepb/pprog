import numpy as np
import scipy.optimize as optim


class ANN:
    def __init__(self, neurons, function, functionPrime):
        self.n = neurons
        self.g = function
        self.gPrime = functionPrime
        self.params = np.reshape(np.random.rand(3 * self.n),(self.n,3))

    def feed_forward(self, x):
        output = np.zeros(np.shape(x))
        outputPrime = np.zeros(np.shape(x))
        for i in range(self.n):
            a = self.params[i][0]
            b = self.params[i][1]
            w = self.params[i][2]
            output += self.g((x - a) / b) * w
            outputPrime += self.gPrime((x - a) / b) * w / b
        return output, outputPrime

    def train(self, inputValues, initialValues, function):
        N = np.shape(inputValues)[0]
        x = inputValues
        x0 = initialValues[0]
        f0 = initialValues[1]
        def delta(p):
            self.params = np.reshape(p, (self.n, 3))
            y,yPrime = self.feed_forward(x)
            y0,y0prime = self.feed_forward(x0)
            fprime=function(x,y)

            deviationFromInitial = N * np.abs(y0-f0)**2
            deviation = np.sum(np.abs(yPrime - fprime) ** 2)
            s = deviation + deviationFromInitial
            return s

        optim.minimize(delta, self.params, method='BFGS', tol=1e-3)
