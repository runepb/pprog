import numpy as np
import ANN
import matplotlib.pyplot as plt

def activatorFunction(x):
    return x*np.exp(-x**2)
def activatorFunctionPrime(x):
    return (1-2*x**2)*np.exp(-x**2)

def logistic(x): #analytical expression
    return np.divide(1, 1+np.exp(-x))
def logisticTrainer(x, y):
    return y*(1-y)


def gauss(x):  #analytical expression
    return np.exp(-x * x / 2)
def gaussTrainer(x, y):
    return -x*y


if __name__ == '__main__':
    neurons = 5
    N=50
    x = np.zeros(N)
    a=-5
    b=5
    print("\n\nrunning ANN, to find the solution to:")
    print("A logistic ODE, written as          : y\'=y*(1-y)   x ∈ [-5,5], y(0)=0.5")
    print("A gaussian ODE, written as          : y\'=-x*y      x ∈ [-5,5], y(0)=0.5")
    print("\nThe activator function for both ANN is a gausian wavelet, written as:")
    print("g(x)  = x*exp(-x²)")
    print("g'(x) = (1-2*x²)*exp(-x²)")
    print("\nThe ANN has 5 hidden neurons and is calculated at 50 x-values between -5 and 5.")
    print("See SVG-files for the plots comparing the analytical functions to the values from the ANN")
    for i in range(N):
        x[i] = a + (b-a)*(i)/(N)
    annLog = ANN.ANN(neurons, activatorFunction,activatorFunctionPrime)
    annGauss = ANN.ANN(neurons, activatorFunction,activatorFunctionPrime)

    p = np.empty([neurons, 3])
    for i in range(neurons):
        p[i][0] = a + (b - a)/(neurons-1) * i
        p[i][1] = 1
        p[i][2] = 1
    annLog.params = p
    annGauss.params = p
    logInitial = np.array([0, 0.5])
    gaussInitial = np.array([0, 1])
    annLog.train(x, logInitial, logisticTrainer)
    annGauss.train(x, gaussInitial, gaussTrainer)
    (yLog, dyLog) = annLog.feed_forward(x)
    (yGauss, dyGauss) = annGauss.feed_forward(x)

    figLog = plt.figure(1)
    plt.title('Logistic function')
    plt.scatter(x, yLog,label='ANN',marker='x',c='red')
    plt.plot(x, logistic(x),label='analytical',c='black')
    plt.legend()
    plt.savefig('LogPlot.svg')

    figGauss = plt.figure(2)
    plt.title('Gaussian function')
    plt.scatter(x, yGauss, label='ANN',marker='x',c='red')
    plt.plot(x, gauss(x),label='analytical',c='black')
    plt.legend()
    plt.savefig('GaussPlot.svg')