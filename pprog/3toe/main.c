#include <stdio.h>
#include <math.h>
#include <limits.h>
#include <float.h>
#include "equal.h"

int main(int argc, char *argv[])
{
	//1 - i
	int i=1; while(i+1>i) {++i;}
	printf("%i while\n",i);
	i=1; do{++i;}while(i+1>i);
	printf("%i do while\n",i);
	for (i = 0; i < i+1; ++i) {}
	printf("%i forloop\n",i);
	printf("%i from INT_MAX\n", INT_MAX);

	//1 - ii
	i=1; while(i>i-1) {--i;}
	printf("\n%i while\n",i);
	i=1; do{--i;}while(i>i-1);
	printf("%i do while\n",i);
	for (i = 0; i-1 < i; --i) {}
	printf("%i forloop\n",i);
	printf("%i from INT_MIN\n", INT_MIN);

	//1 - iii
	float x = 1; while(1+x/2!=1) {x/=2;}
	printf("\n%g while\n",x);
	x = 1; do{x/=2;}while(1+x/2!=1);
	printf("%g do while\n",x);
	for (x = 1; 1+x/2!=1; x/=2) {}
	printf("%g forloop\n",x);
	printf("%g from FLT_EPSILON\n",FLT_EPSILON);

	double y = 1; while(1+y/2!=1) {y/=2;}
	printf("\n%g while\n",y);
	y = 1; do{y/=2;}while(1+y/2!=1);
	printf("%g do while\n",y);
	for (y = 1; 1+y/2!=1; y/=2) {}
	printf("%g forloop\n",y);
	printf("%g from DBL_EPSILON\n",DBL_EPSILON);

	long double z = 1; while(1+z/2!=1) {z/=2;}
	printf("\n%Lg while\n", z);
	z = 1; do{z/=2;}while(1+z/2!=1);
	printf("%Lg dowhile\n", z);
	for (z = 1; 1+z/2!=1; z/=2) {}
	printf("%Lg forloop \n", z);
	printf("%Lg (LDBL_EPSILON)\n",LDBL_EPSILON);

	//2 - i
	int max = INT_MAX/2;
	float sum_up_float = 0;
	for (int i = 1; i < max; ++i) {sum_up_float+=1./i;}
	printf("%g sum_up_float\n", sum_up_float);
	float sum_down_float = 0;
	for (int i = max; i > 0; --i) {sum_down_float+=1./i;}
	printf("%g sum_down_float\n", sum_down_float);

	//2 - ii
	printf("\nThe lowest numbers in the up sum, is rounded down to zero\n");

	//2 - iii
	printf("\nWhem max reaches a number, where the addition is rounded to 0, it converges.\n");

	//2 - iiii
	double sum_up_double = 0;
	for (int i = 1; i < max; ++i) {sum_up_double+=1./i;}
	printf("\n%.20g sum_up_double\n", sum_up_double);
	double sum_down_double = 0;
	for (int i = max; i > 0; --i) {sum_down_double+=1./i;}
	printf("%.20g sum_down_double\n", sum_down_double);

	printf("\n With double, we see a better result, but it still happens.\n");

	//3
	printf("\nequal(1,1,1,1): %i\n",equal(1,1,1,0.5));
	printf("equal(1,2,1,1): %i\n",equal(1,2,1,0.5));
	printf("equal(1,2,2,1): %i\n",equal(1,2,2,0.5));
	printf("equal(1,2,1,2): %i\n",equal(1,2,1,1));
	return 0;
}
