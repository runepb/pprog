#include "stdio.h"
#include <math.h>
#include <complex.h>

int main(){
	double g = tgamma(5.0);
	double b = j0(0.5);
	complex r = csqrt(-2);
	complex i = cexp(I);
	complex p = cexp(M_PI*I);
	complex e = cpow(I,M_E);
	printf("gammafunction of 5 is %g\nBesselfunction of 0.5 is %g\n",g,b);
	printf("sqrt of -2 is %g+%g\nexp(i) is %g+%g\n",creal(r),cimag(r),creal(i),cimag(i));
	printf("exp(i Pi) is %g+%g\ni^e is %g+%g\n",creal(p),cimag(p),creal(e),cimag(e));
	float x1 = 1.0/9;	
	double x2 = 1.0/9;	
	long double x3 = 1.L/9;	
	printf("1/9 for float is %.25g\n1/9 for double is %.25lg\n1/9 for long double is %.25Lg\n",x1,x2,x3);
	return 0;
}
