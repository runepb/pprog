#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<gsl/gsl_multiroots.h>
#include<gsl/gsl_errno.h>
#include<gsl/gsl_odeiv2.h>


int rosenbrokGrad(const gsl_vector * v, void * params, gsl_vector * f){
	gsl_vector_set(f, 0, -2*(1-gsl_vector_get(v,0)) - 400*(gsl_vector_get(v,1) - pow(gsl_vector_get(v,0),2))*gsl_vector_get(v,0));
	gsl_vector_set(f, 1, 200*(gsl_vector_get(v,1) - pow(gsl_vector_get(v,0),2)));
	return GSL_SUCCESS;
}
int ode_schrodinger(double r, const double y[], double dy[], void* params){
	double epsilon = *(double*)params;
	dy[0]=y[1];
	dy[1]= -2*(1/r+epsilon)*y[0];
	return GSL_SUCCESS;
}

double Fe(double epsilon, double r){
	double rmin = 1e-3;
	if(r<rmin) return r-r*r;

	gsl_odeiv2_system diffequation;
	diffequation.function = ode_schrodinger;
	diffequation.dimension = 2;
	diffequation.params = (void*) &epsilon;

	double hstart = 1e-3, epsabs = 1e-6, epsrel = 1e-6;
	gsl_odeiv2_driver* driver = gsl_odeiv2_driver_alloc_y_new (&diffequation, gsl_odeiv2_step_rk8pd, hstart, epsabs, epsrel);

	double t=rmin, y[2] = {t-t*t, 1-2*t};
	gsl_odeiv2_driver_apply (driver, &t, r, y);
	gsl_odeiv2_driver_free (driver);
	return y[0];
}

int shooter(const gsl_vector * x, void * params, gsl_vector * f){
	gsl_vector_set(f, 0, Fe(gsl_vector_get(x, 0), 8.));
	return GSL_SUCCESS;
}


int main(){
	
	printf("\n Exercise 1\n");
	printf("\nRoot of the gradient of the Rosenbrok equation.\n");
	
	const gsl_multiroot_fsolver_type * solverTemp = gsl_multiroot_fsolver_hybrid;
	gsl_multiroot_fsolver * solver = gsl_multiroot_fsolver_alloc (solverTemp, 2);
	gsl_multiroot_function F;
	F.f = &rosenbrokGrad;
	F.n = 2;
	F.params = NULL;
	
	gsl_vector *x = gsl_vector_alloc(2);
	gsl_vector_set(x, 0, 0);
	gsl_vector_set(x, 1, 0);

	printf("Solver x and y values\n");

	gsl_multiroot_fsolver_set(solver, &F, x);
	
	double epsabs = 1e-8;
	int status, num_iterations = 0;
	do{
		gsl_multiroot_fsolver_iterate(solver);
		status = gsl_multiroot_test_residual(solver->f,epsabs);
		num_iterations++;
		printf("(iteration: %i, x=%g,y=%g).\n",num_iterations, gsl_vector_get(solver->x,0), gsl_vector_get(solver->x,1));
	}while(status == GSL_CONTINUE && num_iterations < 5000);
	
	if(num_iterations == 5000) {printf("no convergence after 5000 iterations.");}
	printf("converged at (%g,%g).\n", gsl_vector_get(solver->x,0), gsl_vector_get(solver->x,1));}
	gsl_multiroot_fsolver_free(solver);
	gsl_vector_free(x);


	printf("\n\n exercise 2\n");
	
	FILE* file;
	file = fopen("plot.dat", "w");
	F.f = &shooter;
	F.n = 1;
	F.params = NULL;

	solver = gsl_multiroot_fsolver_alloc (solverTemp, 1);
	x = gsl_vector_alloc(1);
	gsl_vector_set(x, 0, -1);
	
	gsl_multiroot_fsolver_set(solver, &F, x);
	epsabs = 1e-6;
	num_iterations = 0;
	
	do{
		gsl_multiroot_fsolver_iterate(solver);
		status = gsl_multiroot_test_residual(solver->f,epsabs);
		num_iterations++;
	}while(status == GSL_CONTINUE && num_iterations < 5000);

	if(num_iterations == 5000) {printf("no convergence after 5000 iterations.");}
	else{printf("\nRoot finder converged.\n");}
	
	double E =  gsl_vector_get(solver->x,0);


	for(double r = 0; r<8+1e-5; r+=8./1000.){
		fprintf(file,"%g %g\n", r, Fe(E, r));
	}
	fprintf(file, "\n\n");
	for(double r = 0; r<8+1e-5; r+=8./20.){
		fprintf(file,"%g %g\n", r, r*exp(-r));
	}
	

	printf("E = %g\n\n", E);	

	gsl_multiroot_fsolver_free(solver);
	gsl_vector_free(x);
	
	
	return 0;
}













