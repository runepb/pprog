#include <stdio.h>
#include <gsl/gsl_multimin.h>
#include <gsl/gsl_errno.h>
#include <math.h>

struct data{int n; double *t,*y,*e;};

double rosenbrock(const gsl_vector* v, void* params) {
	double x = gsl_vector_get(v,0);
	double y = gsl_vector_get(v,1);
	return pow((1-x),2)+100*pow((y-pow(x,2)),2);
}

double fittingfunction(const gsl_vector* v, void* params) {
	struct data p = *(struct data*) params;
	double A = gsl_vector_get(v,0);
	double B = gsl_vector_get(v,1);
	double T = gsl_vector_get(v,2);
	double sum=0;
	for (int i=0; i<p.n; i++) sum+=pow((A*exp(-p.t[i]/T)+B-p.y[i])/p.e[i],2);
	return sum;
}

int main(int argc, char *argv[]) {
	int dim = 2;
	const gsl_multimin_fminimizer_type * type = gsl_multimin_fminimizer_nmsimplex2;
	gsl_multimin_fminimizer* state = gsl_multimin_fminimizer_alloc (type,dim);
	gsl_multimin_function F = {rosenbrock, dim, NULL};
	gsl_vector* start = gsl_vector_calloc(dim);
	gsl_vector* step = gsl_vector_calloc(dim);
	gsl_vector_set_all(step,0.05);
	gsl_multimin_fminimizer_set(state, &F, start, step);

	int status = GSL_CONTINUE;
	for (int iter=0; status==GSL_CONTINUE && iter<1000; iter++) {
		status = gsl_multimin_fminimizer_iterate (state);
		if(status) break;
		printf("Itearation: %i, x=%g,y=%g\n",iter,gsl_vector_get(state->x,0),gsl_vector_get(state->x,1));

		status = gsl_multimin_test_size (state->size, 0.001);
		if (status == GSL_SUCCESS) printf("converged in %i iterations, at x=%g and y=%g\n",iter,gsl_vector_get(state->x,0),gsl_vector_get(state->x,1));
	}	



	double t[]= {0.47,1.41,2.36,3.30,4.24,5.18,6.13,7.07,8.01,8.95};
	double y[]= {5.49,4.08,3.54,2.61,2.09,1.91,1.55,1.47,1.45,1.25};
	double e[]= {0.26,0.12,0.27,0.10,0.15,0.11,0.13,0.07,0.15,0.09};
	int n = sizeof(t)/sizeof(t[0]);
	struct data p = {n, t, y, e};
	dim = 3;
	state =gsl_multimin_fminimizer_alloc (type,dim);
	gsl_multimin_function F2 = {fittingfunction, dim, &p};
	start = gsl_vector_calloc(dim);
	step = gsl_vector_calloc(dim);
	gsl_vector_set_all(step,5);
	gsl_multimin_fminimizer_set(state, &F2, start, step);
	status = GSL_CONTINUE;
	for (int iter=0; status==GSL_CONTINUE && iter<500; iter++) {
		status = gsl_multimin_fminimizer_iterate(state);
		if(status) break;
		status = gsl_multimin_test_size (state->size, 0.001);
	}
	double A=gsl_vector_get(state->x,0);
	double B=gsl_vector_get(state->x,1);
	double T=gsl_vector_get(state->x,2);
	FILE* file;
	file = fopen("plot.dat", "w");
	for (int i=0; i<n; i++) fprintf(file,"%g %g %g\n",t[i],y[i],A*exp(-t[i]/T)+B);
	return 0;
}

