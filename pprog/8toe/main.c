#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<gsl/gsl_odeiv2.h>
#include<gsl/gsl_errno.h>


int logist_ode(double x,const double y[],double dydx[],void* params){
	dydx[0] = y[0]*(1-y[0]);
	return GSL_SUCCESS;
}

double calculateODE(double x){
	gsl_odeiv2_system difEq;
	difEq.function=logist_ode;
	difEq.jacobian=NULL;
	difEq.dimension=1;
	difEq.params=NULL;
	
	double hstart = 1e-3;
	double acc=1e-6;
	double eps=1e-6;
	gsl_odeiv2_driver* driver = gsl_odeiv2_driver_alloc_y_new(&difEq,gsl_odeiv2_step_rk8pd,hstart,acc,eps);	

	double x_0=0;
	double y[1]={0.5};
	gsl_odeiv2_driver_apply(driver,&x_0,x,y);

	gsl_odeiv2_driver_free(driver);
	return y[0];
}


int orbitEq(double x,const double y[],double dydx[],void* params){
	double epsilon = *(double*) params;
	dydx[0] = y[1];
	dydx[1] = 1 - y[0] + epsilon * y[0] * y[0];
	return GSL_SUCCESS;
}



double calculateOrbit(double x, double init_val[], double rel_cor){
	gsl_odeiv2_system sys;
	sys.function=orbitEq;
	sys.jacobian=NULL;
	sys.dimension=2;
	sys.params = (void*) &rel_cor;
	double y[2]; y[0] = init_val[0]; y[1] = init_val[1];
	
	double hstart= 1e-3;
	double acc=1e-8;
	double eps=1e-8;
	gsl_odeiv2_driver* driver = gsl_odeiv2_driver_alloc_y_new(&sys,gsl_odeiv2_step_rkf45,hstart,acc,eps);	

	double x_0=0;
	gsl_odeiv2_driver_apply(driver,&x_0,x,y);
	
	gsl_odeiv2_driver_free(driver);
	return y[0];
}






int main(){

	printf("\n exercise 1\n");
	printf("Integrating y'(x) = y(x)*(1-y(x)) from x = 0 to x = 3 with:\n initial condition y(0) = 0.5\n eps=1e-6 \n acc=1e-6 \n hstart=1e-3");
	printf("y(3) = %g\n", calculateODE(3));
	printf("The real value being:\n");
	printf("y(3) = %g\n", 1/(1+exp(-3)));
	
	
	printf("\nProblem 2\n");	
	FILE* file = fopen("orbit.dat", "w");
	double init_val[2];
	init_val[0] = 1.; init_val[1] = 0.;
	double rel_cor = 0.0;

	printf("\nIntegrating orbital ode equations with:\n u(0) = 1\n u\'(0) = 0 \n Epsilon=0\n eps=1e-6 \n acc=1e-6 \n hstart=1e-3\n");
	for(int i=0; i <= 100; i++)
		fprintf(file, "%g %g\n", 2*M_PI/100*i, calculateOrbit(2*M_PI/100*i, init_val, rel_cor));
	printf("Done! see orbit1.svg for plot.\n\n");	
	fprintf(file, "\n\n");

	init_val[1] = -0.5;
	printf("\nIntegrating orbital ode equations with:\n u(0) = 1\n u\'(0) = -0.5 \n Epsilon=0\n eps=1e-6 \n acc=1e-6 \n hstart=1e-3\n");
	printf("Solving u(φ)'' + u(φ) = 1 + εu(φ)^2 for an elliptical orbit\nwith u(0) = 1, u(0) = -0.5 and ε = 0\n");
	for(int i=0; i <= 100; i++)
		fprintf(file, "%g %g\n", 2*M_PI/100*i, calculateOrbit(2*M_PI/100*i, init_val, rel_cor));
	printf("Done! see orbit2.svg for plot.\n\n");
	fprintf(file, "\n\n");
	
	rel_cor = 0.02;
	printf("\nIntegrating orbital ode equations with:\n u(0) = 1\n u\'(0) = -0.5 \n Epsilon=0.02\n eps=1e-6 \n acc=1e-6 \n hstart=1e-3\n");
	for(int i=0; i <= 1500; i++)
		fprintf(file, "%g %g\n", 2*M_PI/100*i, calculateOrbit(2*M_PI/100*i, init_val, rel_cor));
	printf("See orbit3.svg for plot.\n\n");
	

	return 0;
}













