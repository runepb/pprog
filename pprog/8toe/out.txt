
 exercise 1
Integrating y'(x) = y(x)*(1-y(x)) from x = 0 to x = 3 with:
 initial condition y(0) = 0.5
 eps=1e-6 
 acc=1e-6 
 hstart=1e-3y(3) = 0.952574
The real value being:
y(3) = 0.952574

Problem 2

Integrating orbital ode equations with:
 u(0) = 1
 u'(0) = 0 
 Epsilon=0
 eps=1e-6 
 acc=1e-6 
 hstart=1e-3
Done! see orbit1.svg for plot.


Integrating orbital ode equations with:
 u(0) = 1
 u'(0) = -0.5 
 Epsilon=0
 eps=1e-6 
 acc=1e-6 
 hstart=1e-3
Solving u(φ)'' + u(φ) = 1 + εu(φ)^2 for an elliptical orbit
with u(0) = 1, u(0) = -0.5 and ε = 0
Done! see orbit2.svg for plot.


Integrating orbital ode equations with:
 u(0) = 1
 u'(0) = -0.5 
 Epsilon=0.02
 eps=1e-6 
 acc=1e-6 
 hstart=1e-3
See orbit3.svg for plot.

