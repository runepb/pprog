#include"nvector.h"
#include<stdio.h>
#include<stdlib.h>
#define RND (double)rand()/RAND_MAX

int main()
{
	int n = 5;

	printf("\nmain: testing nvector_alloc ...\n");
	nvector *v = nvector_alloc(n);
	if (v == NULL) printf("test failed\n");
	else printf("test passed\n");

	printf("\nmain: testing nvector_set and nvector_get ...\n");
	double value = RND;
	int i = n / 2;
	nvector_set(v, i, value);
	double vi = nvector_get(v, i);
	if (vi == value) printf("test passed\n");
	else printf("test failed\n");

	printf("\nmain: testing nvector_dot_product...\n");
	nvector *a = nvector_alloc(n);
	nvector *b = nvector_alloc(n);
	double result = 0;
	for (int i = 0; i < n; i++) {
		double x = RND, y = RND;
		nvector_set(a, i, x);
		nvector_set(b, i, y);
		result=result+x*y;	
	}
	if(nvector_dot_product(a,b)==result) printf("test passed\n");
	else printf("test failed\n");

	nvector_free(v);
	nvector_free(a);
	nvector_free(b);
	return 0;
}
