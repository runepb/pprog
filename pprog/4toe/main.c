#include"komplex.h"
#include<stdio.h>
#define TINY 1e-6

int main(){
	komplex a = {1,2};
	komplex b = {3,4};
	printf("\ntesting komplex_add and print\n");
	komplex r = komplex_add(a,b);
	komplex R = {4,6};
	komplex_print("The adition should be",R);
	komplex_print("The addition using komplex_add is",r);

	printf("\ntesting komplex_sub\n");
	komplex r2 = komplex_sub(b,a);
	komplex R2 = {2,2};
	komplex_print("The substraction should be",R2);
	komplex_print("The substraction using komplex_sub is",r2);

	printf("\ntesting komplex_new and komplex_set\n");
	komplex z = komplex_new(6,2);
	komplex_print("z using komplex_new is",z);
	komplex_set(&z,1,1);
	komplex_print("z has now been sat to 1,1, and is",z);

	printf("\ntesteng komplex_equals\n");
	int isEquals = komplex_equal(a,b);
	printf("a and b is not equal, komplex_equals returns %i\n",isEquals);
	komplex a2 = {3,4};
	komplex b2 = {3,4};
	int isEquals2 = komplex_equal(a2,b2);
	printf("a and b is equal, komplex_equals returns %i\n",isEquals2);
}
	
